<?php declare(strict_types=1);

use Doctrine\Common\Cache\PhpFileCache;
use Dotenv\Dotenv;
use GuzzleHttp\Client;

require __DIR__ . '/../vendor/autoload.php';

(Dotenv::createImmutable(__DIR__ . '/..'))->load();

$cache = new PhpFileCache(__DIR__ . '/../cache');

// Check cache
$feed = $cache->fetch('feed');

$expires_at = date_create('-' . getenv('EXPIRES_IN'));

if (!$feed || $feed['refreshed_at'] < $expires_at) {
    
    $client = new Client();

    $xml = $client->get(getenv('URL'), [
        'http_errors' => false,
    ]);

    if ($xml->getStatusCode() < 400) {
        $feed = [
            'refreshed_at' => date_create_immutable(),
            'fetched_at' => date_create_immutable(),
            
            'xml' => $xml->getBody()->__toString(),
            'failure_count' => 0,
        ];
        
        $cache->save('feed', $feed);    
    } elseif ($feed) {
        $feed['refreshed_at'] = date_create_immutable();
        $feed['failure_count'] += 1;
        
        if ($feed['failure_count'] % 5 == 0) {
            // @todo send email
            error_log(sprintf(
                "Failure to refresh feed [failure_count: %d]",
                $feed['failure_count']
            ));
        }

        $cache->save('feed', $feed);
    } else {
        http_response_code(500);
        echo "Can not fetch feed, and no feed is cached.";
        exit;
    }
}

// load soundcloud xml
$soundcloud = new DOMDocument();
$soundcloud->loadXML($feed['xml'], LIBXML_COMPACT | LIBXML_NOBLANKS | LIBXML_NONET);
$xpath = new DOMXPath($soundcloud);

$process = function ($name, $search) use ($xpath) {
    $xml = file_get_contents(__DIR__ . "/../templates/{$name}.xml");
    $xml = str_replace('{{APP_URL}}', getenv('APP_URL'), $xml);

    $dom = new DOMDocument();
    $dom->loadXML($xml, LIBXML_NONET | LIBXML_COMPACT | LIBXML_NOBLANKS);

    $translate = "translate(title/text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz')";
    $elements = $xpath->query("/rss/channel/item[contains({$translate}, '{$search}')]");

    foreach ($elements as $element) {
        $element = $dom->importNode($element, true);
        $dom->firstChild->firstChild->appendChild($element);
    }

    header('Content-Type: text/plain');
    $dom->preserveWhiteSpace = false;
    $dom->formatOutput = true;

    // @todo don't do anything if hash of existing file matches
    // @todo this will allow Nginx to continue to serve the same
    // @todo eTag/last-modified headers
    $dom->save(__DIR__ . "/{$name}.xml");
};

$process('superego', 'superego');
$process('pistol-shrimps-radio', 'pistol shrimps');

